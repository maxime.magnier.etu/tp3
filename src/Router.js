export default class Router {
	static titleElement;
	static contentElement;
	static routes;
	static #menuElement;

	static navigate(path) {
		const route = this.routes.find(route => route.path === path);
		if (route) {
			this.titleElement.innerHTML = `<h1>${route.title}</h1>`;
			this.contentElement.innerHTML = route.page.render();
			route.page.mount?.(this.contentElement);
		}
	}

	static set menuElement(element) {
		this.#menuElement = element;
		let value = element.querySelectorAll('a');
		console.log(value);

		value.forEach(winnie => {
			winnie.addEventListener('click', event => {
				event.preventDefault();
				this.navigate(winnie.getAttribute('href'));
			});
		});
	}
}
