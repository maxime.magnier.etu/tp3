import Router from './Router';
import data from './data';
import PizzaList from './pages/PizzaList';
import Component from './components/Component.js';

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');
Router.menuElement = document.querySelector('.mainMenu');

const pizzaList = new PizzaList([]),
	aboutPage = new Component('section', null, 'Ce site est génial'),
	pizzaForm = new Component(
		'section',
		null,
		'Ici vous pourrez ajouter une pizza'
	);

Router.routes = [
	{ path: '/', page: pizzaList, title: 'La carte' },
	{ path: '/a-propos', page: aboutPage, title: 'À propos' },
	{ path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
];

Router.navigate('/'); // affiche une page vide
pizzaList.pizzas = data;
Router.navigate('/'); // affiche la liste des pizzas

document.body.querySelector('.newsContainer').setAttribute('style', '');
const close = document.body.querySelector('.newsContainer .closeButton');
close.addEventListener('click', event => {
	document.body
		.querySelector('.newsContainer')
		.setAttribute('style', 'display:none');
});
